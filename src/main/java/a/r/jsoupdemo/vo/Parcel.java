package a.r.jsoupdemo.vo;

/**
 * @author : alif.razak@canang.com.my
 * @since : 4/20/2018 11:54 AM
 */
public class Parcel {
    private String ts;
    private String desc;
    private String location;

    public Parcel(String ts, String desc, String location) {
        this.ts = ts;
        this.desc = desc;
        this.location = location;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
