package a.r.jsoupdemo.controller;

import a.r.jsoupdemo.vo.Parcel;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
public class IndexController {

    //EP492809906MY
    @GetMapping(path = "/poslaju")
    public String poslaju(@RequestParam("q") String trackingNo) throws IOException {
        Document document = Jsoup.connect("https://www.poslaju.com.my/track-trace-v2/")
                .data("trackingNo03", trackingNo)
                .data("hvtrackNoHeader03", "")
                .data("hvfromheader03", "0")
                .post();

        String data = document.data();

        Pattern pattern = Pattern.compile("<table id='tbDetails' class=[\\W\\w]+>([\\s\\S.]*)</table>");
        Matcher matcher = pattern.matcher(data);
        ObjectMapper objectMapper = new ObjectMapper();
        List<Parcel> strings = new ArrayList<>();
        while (matcher.find()) {
            Element html = document.html(matcher.group(0));
            Elements items = html.select("tbody > tr");

            for (Element item : items) {
                Elements tds = item.select("td");

                Element ts = tds.get(0);
                Element desc = tds.get(1);
                Element loc = tds.get(2);

                String tsTxt = StringUtils.rightPad(ts.text(), 30, " ");
                String descTxt = StringUtils.rightPad(ellipsis(desc.text()), 50, " ");
                String locTxt = StringUtils.rightPad(loc.text(), 50, " ");

                Parcel parcel = new Parcel(tsTxt, descTxt, locTxt);
                strings.add(parcel);

            }
        }
        return objectMapper.writeValueAsString(strings);
    }

    private static String ellipsis(String text) {
        if (text.length() > 30)
            text = text.substring(0, 30).concat("...");
        return text;
    }

}
